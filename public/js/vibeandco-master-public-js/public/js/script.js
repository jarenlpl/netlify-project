// main js doc


// main js file


// update copyright year

const copyrightYear = document.querySelector('#copyrightYear');

function copyrightUpdate() {
    let dt = new Date().getFullYear();
    copyrightYear.innerHTML = dt;
}
copyrightUpdate();


//menu open/close
const openBtn = document.querySelector('#openMenuBtn');
const closeBtn = document.querySelector('#closeMenuBtn');

const navContainer = document.querySelector('#mainNavContainer');

function openMenu() {
    //console.log('click');
    window.scrollTo(0, 0);

    navContainer.style.opacity = "1";
    navContainer.style.right = "0";
}

function closeMenu() {
    //console.log('click2');

    navContainer.style.opacity = "0";
    navContainer.style.right = "-100%";
}


openBtn.addEventListener('click', openMenu, false);
openBtn.addEventListener('onKeyPress', openMenu, false);

closeBtn.addEventListener('click', closeMenu, false);
closeBtn.addEventListener('onKeyPress', closeMenu, false);


//back to top appear
const topBtn = document.querySelector('#topBtn');

window.addEventListener("scroll", function(){
    if(window.scrollY > 800) {
        topBtn.style.opacity = "1";
        topBtn.style.right = "3%";
    } else {
        topBtn.style.opacity = "0";
        topBtn.style.right = "-83px";
    }
}, false);
