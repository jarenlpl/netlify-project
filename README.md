# Netlify Test Site

[![Netlify Status](https://api.netlify.com/api/v1/badges/86aef4dc-b50a-4fa6-8a3d-32f9caddf2f1/deploy-status)](https://app.netlify.com/sites/simple-starter-jmlpl/deploys)

Simple test of Netlify services with Gitlab

View site here: [simple-starter-jmlpl.netlify.com](https://simple-starter-jmlpl.netlify.com/)
